package auth

import (
	"context"
	"gitlab.com/timofeev.pavel.art/rpc_project-auth_api/internal/modules/auth/service"
	"gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/pkg/processing/auth"
	pb "gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/pkg/proto/auth_grpc"
)

type AuthServiceGRPC struct {
	authService service.Auther
	pb.UnimplementedAutherServer
}

func NewAuthServiceGRPC(authService service.Auther) *AuthServiceGRPC {
	return &AuthServiceGRPC{authService: authService}
}

func (a *AuthServiceGRPC) Register(ctx context.Context, request *pb.RegisterRequest) (*pb.RegisterResponse, error) {

	out := a.authService.Register(ctx, auth.RegisterIn{
		Email:          request.GetEmail(),
		Phone:          request.GetPhone(),
		Password:       request.GetPassword(),
		IdempotencyKey: request.GetIdempotencyKey(),
	})

	return &pb.RegisterResponse{
		Status:    int64(out.Status),
		ErrorCode: int64(out.ErrorCode),
	}, nil
}

func (a *AuthServiceGRPC) AuthorizeEmail(ctx context.Context, request *pb.AuthorizeEmailRequest) (*pb.AuthorizeResponse, error) {

	out := a.authService.AuthorizeEmail(ctx, auth.AuthorizeEmailIn{
		Email:          request.GetEmail(),
		Password:       request.GetPassword(),
		RetypePassword: request.GetRetypePassword(),
	})

	return &pb.AuthorizeResponse{
		UserID:       int64(out.UserID),
		AccessToken:  out.AccessToken,
		RefreshToken: out.RefreshToken,
		ErrorCode:    int64(out.ErrorCode),
	}, nil
}

func (a *AuthServiceGRPC) AuthorizeRefresh(ctx context.Context, request *pb.AuthorizeRefreshRequest) (*pb.AuthorizeResponse, error) {

	out := a.authService.AuthorizeRefresh(ctx, auth.AuthorizeRefreshIn{UserID: int(request.GetUserID())})

	return &pb.AuthorizeResponse{
		UserID:       int64(out.UserID),
		AccessToken:  out.AccessToken,
		RefreshToken: out.RefreshToken,
		ErrorCode:    int64(out.ErrorCode),
	}, nil
}

func (a *AuthServiceGRPC) AuthorizePhone(ctx context.Context, request *pb.AuthorizePhoneRequest) (*pb.AuthorizeResponse, error) {

	out := a.authService.AuthorizePhone(ctx, auth.AuthorizePhoneIn{
		Phone: request.GetPhone(),
		Code:  int(request.GetCode()),
	})

	return &pb.AuthorizeResponse{
		UserID:       int64(out.UserID),
		AccessToken:  out.AccessToken,
		RefreshToken: out.RefreshToken,
		ErrorCode:    int64(out.ErrorCode),
	}, nil
}

func (a *AuthServiceGRPC) SendPhoneCode(ctx context.Context, request *pb.SendPhoneCodeRequest) (*pb.SendPhoneCodeResponse, error) {

	out := a.authService.SendPhoneCode(ctx, auth.SendPhoneCodeIn{Phone: request.GetPhone()})

	return &pb.SendPhoneCodeResponse{
		Phone:     out.Phone,
		Code:      int64(out.Code),
		ErrorCode: int64(out.ErrorCode),
	}, nil
}

func (a *AuthServiceGRPC) VerifyEmail(ctx context.Context, request *pb.VerifyEmailRequest) (*pb.VerifyEmailResponse, error) {

	out := a.authService.VerifyEmail(ctx, auth.VerifyEmailIn{
		Hash:  request.GetHash(),
		Email: request.GetEmail(),
	})

	return &pb.VerifyEmailResponse{
		Success:   out.Success,
		ErrorCode: int64(out.ErrorCode),
	}, nil
}
