package storages

import (
	"gitlab.com/timofeev.pavel.art/rpc_project-auth_api/internal/db/adapter"
	vstorage "gitlab.com/timofeev.pavel.art/rpc_project-auth_api/internal/modules/auth/storage"
)

type Storages struct {
	Verify vstorage.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter) *Storages {
	return &Storages{
		Verify: vstorage.NewEmailVerify(sqlAdapter),
	}
}
