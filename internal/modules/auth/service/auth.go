package service

import (
	"context"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"gitlab.com/timofeev.pavel.art/rpc_project-auth_api/config"
	"gitlab.com/timofeev.pavel.art/rpc_project-auth_api/internal/infrastructure/component"
	"gitlab.com/timofeev.pavel.art/rpc_project-auth_api/internal/infrastructure/errors"
	iservice "gitlab.com/timofeev.pavel.art/rpc_project-auth_api/internal/infrastructure/service"
	"gitlab.com/timofeev.pavel.art/rpc_project-auth_api/internal/infrastructure/tools/cryptography"
	"gitlab.com/timofeev.pavel.art/rpc_project-auth_api/internal/modules/auth/storage"
	uservice "gitlab.com/timofeev.pavel.art/rpc_project-auth_api/internal/modules/user/service"
	"gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/pkg/models"
	"gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/pkg/processing/auth"
	uproc "gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/pkg/processing/user"
	"go.uber.org/zap"
)

type Auth struct {
	conf         config.AppConf
	user         uservice.Userer
	verify       storage.Verifier
	notify       iservice.Notifier
	tokenManager cryptography.TokenManager
	hash         cryptography.Hasher
	logger       *zap.Logger
}

func NewAuth(user uservice.Userer, verify storage.Verifier, components *component.Components) *Auth {
	return &Auth{conf: components.Conf,
		user:         user,
		verify:       verify,
		notify:       components.Notify,
		tokenManager: components.TokenManager,
		hash:         components.Hash,
		logger:       components.Logger,
	}
}

func (a *Auth) Register(ctx context.Context, in auth.RegisterIn) auth.RegisterOut {
	hashPass, err := cryptography.HashPassword(in.Password)
	if err != nil {
		return auth.RegisterOut{
			Status:    http.StatusInternalServerError,
			ErrorCode: errors.HashPasswordError,
		}
	}

	userCreate := uproc.UserCreateIn{
		Email:    in.Email,
		Password: hashPass,
	}

	userOut := a.user.Create(ctx, userCreate)
	if userOut.ErrorCode != errors.NoError {
		if userOut.ErrorCode == errors.UserServiceUserAlreadyExists {
			return auth.RegisterOut{
				Status:    http.StatusConflict,
				ErrorCode: userOut.ErrorCode,
			}
		}
		return auth.RegisterOut{
			Status:    http.StatusInternalServerError,
			ErrorCode: userOut.ErrorCode,
		}
	}
	user := a.user.GetByEmail(ctx, uproc.GetByEmailIn{Email: in.Email})
	if user.ErrorCode != errors.NoError {
		return auth.RegisterOut{
			Status:    http.StatusInternalServerError,
			ErrorCode: userOut.ErrorCode,
		}
	}

	hash := a.hash.GenHashString(nil, cryptography.UUID)
	err = a.verify.Create(ctx, in.Email, hash, user.User.ID)
	if err != nil {
		return auth.RegisterOut{
			Status:    http.StatusInternalServerError,
			ErrorCode: http.StatusInternalServerError,
		}
	}
	go a.sendEmailVerifyLink(in.Email, hash)

	return auth.RegisterOut{
		Status:    http.StatusOK,
		ErrorCode: errors.NoError,
	}
}

func (a *Auth) sendEmailVerifyLink(email, hash string) int {
	userOut := a.user.GetByEmail(context.Background(), uproc.GetByEmailIn{Email: email})
	if userOut.ErrorCode != errors.NoError {
		return userOut.ErrorCode
	}

	u, err := url.Parse("http://bing.com/verify?email=sample&hash=sample")
	if err != nil {
		a.logger.Fatal("auth_mess: url parse err", zap.Error(err))

		return errors.AuthUrlParseErr
	}
	u.Scheme = "https"
	if a.conf.Environment != "production" {
		u.Scheme = "http"
	}
	u.Host = a.conf.Domain
	q := u.Query()
	q.Set("email", email)
	q.Set("hash", hash)
	u.RawQuery = q.Encode()
	a.notifyEmail(iservice.PushIn{
		Identifier: email,
		Type:       iservice.PushEmail,
		Title:      "Activation Link",
		Data:       []byte(u.String()),
		Options:    nil,
	})

	return errors.NoError
}

// TODO: Refactor
func (a *Auth) notifyEmail(p iservice.PushIn) {
	res := a.notify.Push(p)
	if res.ErrorCode != errors.NoError {
		time.Sleep(1 * time.Minute)
		go a.notifyEmail(p)
	}
}

func (a *Auth) AuthorizeEmail(ctx context.Context, in auth.AuthorizeEmailIn) auth.AuthorizeOut {
	userOut := a.user.GetByEmail(ctx, uproc.GetByEmailIn{Email: in.Email})
	if userOut.ErrorCode != errors.NoError {
		return auth.AuthorizeOut{
			ErrorCode: userOut.ErrorCode,
		}
	}
	user := userOut.User
	if !cryptography.CheckPassword(user.Password, in.Password) {
		return auth.AuthorizeOut{
			ErrorCode: errors.AuthServiceWrongPasswordErr,
		}
	}
	if !user.EmailVerified {
		return auth.AuthorizeOut{
			ErrorCode: errors.AuthServiceUserNotVerified,
		}
	}

	accessToken, refreshToken, errorCode := a.generateTokens(user)
	if errorCode != errors.NoError {
		return auth.AuthorizeOut{
			ErrorCode: errorCode,
		}
	}

	return auth.AuthorizeOut{
		UserID:       user.ID,
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}
}

func (a *Auth) AuthorizeRefresh(ctx context.Context, in auth.AuthorizeRefreshIn) auth.AuthorizeOut {
	userOut := a.user.GetByID(ctx, uproc.GetByIDIn{UserID: in.UserID})
	if userOut.ErrorCode != errors.NoError {
		return auth.AuthorizeOut{
			ErrorCode: userOut.ErrorCode,
		}
	}
	user := userOut.User

	accessToken, refreshToken, errorCode := a.generateTokens(user)
	if errorCode != errors.NoError {
		return auth.AuthorizeOut{
			ErrorCode: errorCode,
		}
	}

	return auth.AuthorizeOut{
		UserID:       user.ID,
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}
}

func (a *Auth) generateTokens(user *models.User) (string, string, int) {
	accessToken, err := a.tokenManager.CreateToken(
		strconv.Itoa(user.ID),
		strconv.Itoa(user.Role),
		"",
		a.conf.Token.AccessTTL,
		cryptography.AccessToken,
	)
	if err != nil {
		a.logger.Error("auth_mess: create access token err", zap.Error(err))
		return "", "", errors.AuthServiceAccessTokenGenerationErr
	}
	refreshToken, err := a.tokenManager.CreateToken(
		strconv.Itoa(user.ID),
		strconv.Itoa(user.Role),
		"",
		a.conf.Token.RefreshTTL,
		cryptography.RefreshToken,
	)
	if err != nil {
		a.logger.Error("auth_mess: create access token err", zap.Error(err))
		return "", "", errors.AuthServiceRefreshTokenGenerationErr
	}

	return accessToken, refreshToken, errors.NoError
}

func (a *Auth) AuthorizePhone(ctx context.Context, in auth.AuthorizePhoneIn) auth.AuthorizeOut {
	return auth.AuthorizeOut{}
}

func (a *Auth) SendPhoneCode(ctx context.Context, in auth.SendPhoneCodeIn) auth.SendPhoneCodeOut {
	panic("asfasf")
}

func (a *Auth) VerifyEmail(ctx context.Context, in auth.VerifyEmailIn) auth.VerifyEmailOut {
	dto, err := a.verify.GetByEmail(ctx, in.Email, in.Hash)
	if err != nil {
		return auth.VerifyEmailOut{
			ErrorCode: errors.AuthServiceVerifyErr,
		}
	}
	err = a.verify.VerifyEmail(ctx, dto.GetEmail(), dto.GetHash())
	if err != nil {
		return auth.VerifyEmailOut{
			ErrorCode: errors.AuthServiceVerifyErr,
		}
	}
	out := a.user.VerifyEmail(ctx, uproc.UserVerifyEmailIn{
		UserID: dto.GetUserID(),
	})

	return auth.VerifyEmailOut{
		Success:   out.Success,
		ErrorCode: out.ErrorCode,
	}
}
