package service

import (
	"context"
	"gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/pkg/processing/auth"
)

type Auther interface {
	Register(ctx context.Context, in auth.RegisterIn) auth.RegisterOut
	AuthorizeEmail(ctx context.Context, in auth.AuthorizeEmailIn) auth.AuthorizeOut
	AuthorizeRefresh(ctx context.Context, in auth.AuthorizeRefreshIn) auth.AuthorizeOut
	AuthorizePhone(ctx context.Context, in auth.AuthorizePhoneIn) auth.AuthorizeOut
	SendPhoneCode(ctx context.Context, in auth.SendPhoneCodeIn) auth.SendPhoneCodeOut
	VerifyEmail(ctx context.Context, in auth.VerifyEmailIn) auth.VerifyEmailOut
}
