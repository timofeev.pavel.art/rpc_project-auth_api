package controller

import (
	"github.com/ptflp/godecoder"
	"gitlab.com/timofeev.pavel.art/rpc_project-auth_api/internal/infrastructure/component"
	"gitlab.com/timofeev.pavel.art/rpc_project-auth_api/internal/infrastructure/errors"
	"gitlab.com/timofeev.pavel.art/rpc_project-auth_api/internal/infrastructure/handlers"
	"gitlab.com/timofeev.pavel.art/rpc_project-auth_api/internal/infrastructure/responder"
	"gitlab.com/timofeev.pavel.art/rpc_project-auth_api/internal/modules/auth/service"
	"gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/pkg/message/auth_mess"
	"gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/pkg/processing/auth"
	"net/http"
	"net/mail"
)

type Auther interface {
	Register(http.ResponseWriter, *http.Request)
	Login(http.ResponseWriter, *http.Request)
	Refresh(w http.ResponseWriter, r *http.Request)
	Verify(w http.ResponseWriter, r *http.Request)
}

type Auth struct {
	auth service.Auther
	responder.Responder
	godecoder.Decoder
}

func NewAuth(service service.Auther, components *component.Components) Auther {
	return &Auth{auth: service, Responder: components.Responder, Decoder: components.Decoder}
}

func valid(email string) bool {
	_, err := mail.ParseAddress(email)
	return err == nil
}

func (a *Auth) Register(w http.ResponseWriter, r *http.Request) {
	var req auth_mess.RegisterRequest
	err := a.Decode(r.Body, &req)
	if err != nil {
		a.ErrorBadRequest(w, err)
		return
	}

	if !valid(req.Email) {
		a.OutputJSON(w, auth_mess.RegisterResponse{
			Success:   false,
			ErrorCode: http.StatusBadRequest,
			Data: auth_mess.Data{
				Message: "invalid email",
			},
		})
		return
	}

	if req.Password != req.RetypePassword {
		a.OutputJSON(w, auth_mess.RegisterResponse{
			Success:   false,
			ErrorCode: http.StatusBadRequest,
			Data: auth_mess.Data{
				Message: "passwords mismatch",
			},
		})
		return
	}

	out := a.auth.Register(r.Context(), auth.RegisterIn{
		Email:    req.Email,
		Password: req.Password,
	})

	if out.ErrorCode != errors.NoError {
		msg := "register error"
		if out.ErrorCode == errors.UserServiceUserAlreadyExists {
			msg = "User already exists, please check your email"
		}
		a.OutputJSON(w, auth_mess.RegisterResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Data: auth_mess.Data{
				Message: msg,
			},
		})
		return
	}

	a.OutputJSON(w, auth_mess.RegisterResponse{
		Success: true,
		Data: auth_mess.Data{
			Message: "verification link sent to " + req.Email,
		},
	})
}

func (a *Auth) Login(w http.ResponseWriter, r *http.Request) {
	var req auth_mess.LoginRequest
	err := a.Decode(r.Body, &req)
	if err != nil {
		a.ErrorBadRequest(w, err)
		return
	}
	if len(req.Email) < 5 {
		a.OutputJSON(w, auth_mess.RegisterResponse{
			Success:   false,
			ErrorCode: http.StatusBadRequest,
			Data: auth_mess.Data{
				Message: "phone or email empty",
			},
		})
	}

	out := a.auth.AuthorizeEmail(r.Context(), auth.AuthorizeEmailIn{
		Email:    req.Email,
		Password: req.Password,
	})
	if out.ErrorCode == errors.AuthServiceUserNotVerified {
		a.OutputJSON(w, auth_mess.AuthResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Data: auth_mess.LoginData{
				Message: "user_mess email is not verified",
			},
		})
		return
	}

	if out.ErrorCode != errors.NoError {
		a.OutputJSON(w, auth_mess.AuthResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Data: auth_mess.LoginData{
				Message: "login or password mismatch",
			},
		})
		return
	}

	a.OutputJSON(w, auth_mess.AuthResponse{
		Success: true,
		Data: auth_mess.LoginData{
			Message:      "success login",
			AccessToken:  out.AccessToken,
			RefreshToken: out.RefreshToken,
		},
	})
}

func (a *Auth) Refresh(w http.ResponseWriter, r *http.Request) {
	claims, err := handlers.ExtractUser(r)
	if err != nil {
		a.ErrorBadRequest(w, err)
		return
	}
	out := a.auth.AuthorizeRefresh(r.Context(), auth.AuthorizeRefreshIn{UserID: claims.ID})

	if out.ErrorCode != errors.NoError {
		a.OutputJSON(w, auth_mess.AuthResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Data: auth_mess.LoginData{
				Message: "login or password mismatch",
			},
		})
		return
	}

	a.OutputJSON(w, auth_mess.AuthResponse{
		Success: true,
		Data: auth_mess.LoginData{
			Message:      "success refresh",
			AccessToken:  out.AccessToken,
			RefreshToken: out.RefreshToken,
		},
	})
}

func (a *Auth) Verify(w http.ResponseWriter, r *http.Request) {
	var req auth_mess.VerifyRequest
	err := a.Decode(r.Body, &req)
	if err != nil {
		a.ErrorBadRequest(w, err)
		return
	}
	if len(req.Email) < 5 {
		a.OutputJSON(w, auth_mess.RegisterResponse{
			Success:   false,
			ErrorCode: http.StatusBadRequest,
			Data: auth_mess.Data{
				Message: "phone or email empty",
			},
		})
	}

	out := a.auth.VerifyEmail(r.Context(), auth.VerifyEmailIn{
		Email: req.Email,
		Hash:  req.Hash,
	})

	if out.ErrorCode != errors.NoError {
		a.OutputJSON(w, auth_mess.AuthResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Data: auth_mess.LoginData{
				Message: "login or password mismatch",
			},
		})
		return
	}

	a.OutputJSON(w, auth_mess.AuthResponse{
		Success: true,
		Data: auth_mess.LoginData{
			Message: "email verification success",
		},
	})
}
