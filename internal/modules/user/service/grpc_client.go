package service

import (
	"context"

	"gitlab.com/timofeev.pavel.art/rpc_project-auth_api/internal/infrastructure/errors"
	"gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/pkg/models"
	"gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/pkg/processing/user"
	upb "gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/pkg/proto/user_grpc"
)

type UserServiceGRPC struct {
	client upb.UsererClient
}

func NewUserServiceGRPC(client upb.UsererClient) *UserServiceGRPC {
	return &UserServiceGRPC{client: client}
}

func (u *UserServiceGRPC) Create(ctx context.Context, in user.UserCreateIn) user.UserCreateOut {
	resp, err := u.client.CreateUser(ctx, &upb.UserCreateRequest{
		Name:     in.Name,
		Phone:    in.Phone,
		Email:    in.Email,
		Password: in.Password,
		Role:     int64(in.Role),
	})

	if err != nil {
		return user.UserCreateOut{ErrorCode: errors.UserServiceCreateUserErr}
	}

	return user.UserCreateOut{
		UserID:    int(resp.UserID),
		ErrorCode: int(resp.ErrorCode),
	}
}

func (u *UserServiceGRPC) Update(ctx context.Context, in user.UserUpdateIn) user.UserUpdateOut {
	fields := make([]int64, len(in.Fields))
	if in.Fields != nil {
		for i, item := range in.Fields {
			fields[i] = int64(item)
		}
	}

	resp, err := u.client.UdpateUser(ctx, &upb.UserUpdateRequest{
		User: &upb.User{
			ID:            int64(in.User.ID),
			Name:          in.User.Name,
			Phone:         in.User.Phone,
			Email:         in.User.Email,
			Password:      in.User.Password,
			Role:          int64(in.User.Role),
			Verified:      in.User.Verified,
			EmailVerified: in.User.EmailVerified,
			PhoneVerified: in.User.PhoneVerified,
		},
		Fields: fields,
	})

	if err != nil {
		return user.UserUpdateOut{ErrorCode: errors.UserServiceUpdateErr}
	}

	return user.UserUpdateOut{
		Success:   resp.Success,
		ErrorCode: int(resp.ErrorCode),
	}
}

func (u *UserServiceGRPC) VerifyEmail(ctx context.Context, in user.UserVerifyEmailIn) user.UserUpdateOut {
	resp, err := u.client.VerifyEmail(ctx, &upb.UserVerifyEmailRequest{UserID: int64(in.UserID)})

	if err != nil {
		return user.UserUpdateOut{ErrorCode: errors.AuthServiceUserNotVerified}
	}

	return user.UserUpdateOut{
		Success:   resp.Success,
		ErrorCode: int(resp.ErrorCode),
	}
}

func (u *UserServiceGRPC) ChangePassword(ctx context.Context, in user.ChangePasswordIn) user.ChangePasswordOut {
	resp, err := u.client.ChangePassword(ctx, &upb.UserPasswordRequest{
		UserID:      int64(in.UserID),
		OldPassword: in.OldPassword,
		NewPassword: in.NewPassword,
	})

	if err != nil {
		return user.ChangePasswordOut{ErrorCode: errors.UserServiceChangePasswordErr}
	}

	return user.ChangePasswordOut{
		Success:   resp.Success,
		ErrorCode: int(resp.ErrorCode),
	}
}

func (u *UserServiceGRPC) GetByEmail(ctx context.Context, in user.GetByEmailIn) user.UserOut {
	resp, err := u.client.GetUserByEmail(ctx, &upb.UserGetByEmailRequest{Email: in.Email})

	if err != nil {
		return user.UserOut{ErrorCode: errors.UserServiceGetByEmailError}
	}

	return user.UserOut{
		User: &models.User{
			ID:            int(resp.User.GetID()),
			Name:          resp.User.GetName(),
			Phone:         resp.User.GetPhone(),
			Email:         resp.User.GetEmail(),
			Password:      resp.User.GetPassword(),
			Role:          int(resp.User.GetRole()),
			Verified:      resp.User.GetVerified(),
			EmailVerified: resp.User.GetEmailVerified(),
			PhoneVerified: resp.User.GetPhoneVerified(),
		},
		ErrorCode: int(resp.ErrorCode),
	}
}

func (u *UserServiceGRPC) GetByPhone(ctx context.Context, in user.GetByPhoneIn) user.UserOut {
	resp, err := u.client.GetUserByPhone(ctx, &upb.UserGetUserByPhoneRequest{Phone: in.Phone})

	if err != nil {
		return user.UserOut{ErrorCode: errors.UserServiceGetByPhoneError}
	}

	return user.UserOut{
		User: &models.User{
			ID:            int(resp.User.GetID()),
			Name:          resp.User.GetName(),
			Phone:         resp.User.GetPhone(),
			Email:         resp.User.GetEmail(),
			Password:      resp.User.GetPassword(),
			Role:          int(resp.User.GetRole()),
			Verified:      resp.User.GetVerified(),
			EmailVerified: resp.User.GetEmailVerified(),
			PhoneVerified: resp.User.GetPhoneVerified(),
		},
		ErrorCode: int(resp.ErrorCode),
	}
}

func (u *UserServiceGRPC) GetByID(ctx context.Context, in user.GetByIDIn) user.UserOut {
	resp, err := u.client.GetUserByID(ctx, &upb.UserGetUserByIDRequest{UserID: int64(in.UserID)})

	if err != nil {
		return user.UserOut{ErrorCode: errors.UserServiceGetByIDError}
	}

	return user.UserOut{
		User: &models.User{
			ID:            int(resp.User.GetID()),
			Name:          resp.User.GetName(),
			Phone:         resp.User.GetPhone(),
			Email:         resp.User.GetEmail(),
			Password:      resp.User.GetPassword(),
			Role:          int(resp.User.GetRole()),
			Verified:      resp.User.GetVerified(),
			EmailVerified: resp.User.GetEmailVerified(),
			PhoneVerified: resp.User.GetPhoneVerified(),
		},
		ErrorCode: int(resp.ErrorCode),
	}
}

func (u *UserServiceGRPC) GetByIDs(ctx context.Context, in user.GetByIDsIn) user.UsersOut {
	userIds := make([]int64, len(in.UserIDs))
	users := make([]models.User, len(in.UserIDs))

	for i, item := range in.UserIDs {
		userIds[i] = int64(item)
	}

	resp, err := u.client.GetUsersByID(ctx, &upb.UserGetUsersByIDRequest{UserIDs: userIds})

	if err != nil {
		return user.UsersOut{ErrorCode: errors.UserServiceGetByIDsError}
	}

	for i, item := range resp.User {
		users[i] = models.User{
			ID:            int(item.ID),
			Name:          item.Name,
			Phone:         item.Phone,
			Email:         item.Email,
			Password:      item.Password,
			Role:          int(item.Role),
			Verified:      item.Verified,
			EmailVerified: item.EmailVerified,
			PhoneVerified: item.PhoneVerified,
		}
	}

	return user.UsersOut{
		User:      users,
		ErrorCode: int(resp.ErrorCode),
	}
}
