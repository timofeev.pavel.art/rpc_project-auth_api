package modules

import (
	"gitlab.com/timofeev.pavel.art/rpc_project-auth_api/internal/infrastructure/component"
	acontroller "gitlab.com/timofeev.pavel.art/rpc_project-auth_api/internal/modules/auth/controller"
)

type Controllers struct {
	Auth acontroller.Auther
}

func NewControllers(services *Services, components *component.Components) *Controllers {
	authController := acontroller.NewAuth(services.Auth, components)

	return &Controllers{
		Auth: authController,
	}
}
