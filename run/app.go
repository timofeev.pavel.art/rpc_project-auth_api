package run

import (
	"context"
	"fmt"
	"gitlab.com/timofeev.pavel.art/rpc_project-auth_api/rpc/auth"
	apb "gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/pkg/proto/auth_grpc"
	upb "gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/pkg/proto/user_grpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"net/http"
	"net/rpc"
	"net/rpc/jsonrpc"
	"os"

	"github.com/go-chi/chi/v5"
	jsoniter "github.com/json-iterator/go"
	"github.com/ptflp/godecoder"
	"gitlab.com/timofeev.pavel.art/rpc_project-auth_api/config"
	"gitlab.com/timofeev.pavel.art/rpc_project-auth_api/internal/db"
	"gitlab.com/timofeev.pavel.art/rpc_project-auth_api/internal/infrastructure/component"
	"gitlab.com/timofeev.pavel.art/rpc_project-auth_api/internal/infrastructure/db/migrate"
	"gitlab.com/timofeev.pavel.art/rpc_project-auth_api/internal/infrastructure/db/scanner"
	"gitlab.com/timofeev.pavel.art/rpc_project-auth_api/internal/infrastructure/errors"
	"gitlab.com/timofeev.pavel.art/rpc_project-auth_api/internal/infrastructure/responder"
	"gitlab.com/timofeev.pavel.art/rpc_project-auth_api/internal/infrastructure/router"
	"gitlab.com/timofeev.pavel.art/rpc_project-auth_api/internal/infrastructure/server"
	internal "gitlab.com/timofeev.pavel.art/rpc_project-auth_api/internal/infrastructure/service"
	"gitlab.com/timofeev.pavel.art/rpc_project-auth_api/internal/infrastructure/tools/cryptography"
	"gitlab.com/timofeev.pavel.art/rpc_project-auth_api/internal/models"
	"gitlab.com/timofeev.pavel.art/rpc_project-auth_api/internal/modules"
	"gitlab.com/timofeev.pavel.art/rpc_project-auth_api/internal/provider"
	"gitlab.com/timofeev.pavel.art/rpc_project-auth_api/internal/storages"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
)

// Application - интерфейс приложения
type Application interface {
	Runner
	Bootstraper
}

// Runner - интерфейс запуска приложения
type Runner interface {
	Run() int
}

// Bootstraper - интерфейс инициализации приложения
type Bootstraper interface {
	Bootstrap(options ...interface{}) Runner
}

// App - структура приложения
type App struct {
	conf     config.AppConf
	logger   *zap.Logger
	srv      server.Server
	jsonRPC  server.Server
	Sig      chan os.Signal
	Storages *storages.Storages
	Servises *modules.Services
}

// NewApp - конструктор приложения
func NewApp(conf config.AppConf, logger *zap.Logger) *App {
	return &App{conf: conf, logger: logger, Sig: make(chan os.Signal, 1)}
}

// Run - запуск приложения
func (a *App) Run() int {
	// на русском
	// создаем контекст для graceful shutdown
	ctx, cancel := context.WithCancel(context.Background())

	errGroup, ctx := errgroup.WithContext(ctx)

	// запускаем горутину для graceful shutdown
	// при получении сигнала SIGINT
	// вызываем cancel для контекста
	errGroup.Go(func() error {
		sigInt := <-a.Sig
		a.logger.Info("signal interrupt recieved", zap.Stringer("os_signal", sigInt))
		cancel()
		return nil
	})

	// запускаем http сервер
	errGroup.Go(func() error {
		err := a.srv.Serve(ctx)
		if err != nil && err != http.ErrServerClosed {
			a.logger.Error("app: server error", zap.Error(err))
			return err
		}
		return nil
	})

	// запускаем json rpc сервер
	errGroup.Go(func() error {
		err := a.jsonRPC.Serve(ctx)
		if err != nil {
			a.logger.Error("app: server error", zap.Error(err))
			return err
		}
		return nil
	})

	if err := errGroup.Wait(); err != nil {
		return errors.GeneralError
	}

	return errors.NoError
}

// Bootstrap - инициализация приложения
func (a *App) Bootstrap(options ...interface{}) Runner {
	// на русском
	// инициализация емейл провайдера
	email := provider.NewEmail(a.conf.Provider.Email, a.logger)
	// инициализация сервиса нотификации
	notifyEmail := internal.NewNotify(a.conf.Provider.Email, email, a.logger)
	// инициализация менеджера токенов
	tokenManager := cryptography.NewTokenJWT(a.conf.Token)
	// инициализация декодера
	decoder := godecoder.NewDecoder(jsoniter.Config{
		EscapeHTML:             true,
		SortMapKeys:            true,
		ValidateJsonRawMessage: true,
		DisallowUnknownFields:  true,
	})
	// инициализация менеджера ответов сервера
	responseManager := responder.NewResponder(decoder, a.logger)
	// инициализация генератора uuid
	uuID := cryptography.NewUUIDGenerator()
	// инициализация хешера
	hash := cryptography.NewHash(uuID)
	// инициализация компонентов
	components := component.NewComponents(a.conf, notifyEmail, tokenManager, responseManager, decoder, hash, a.logger)
	// инициализация сканера таблиц
	tableScanner := scanner.NewTableScanner()
	// регистрация таблиц
	tableScanner.RegisterTable(
		&models.EmailVerifyDTO{},
	)
	// инициализация базы данных sql и его адаптера
	dbx, sqlAdapter, err := db.NewSqlDB(a.conf.DB, tableScanner, a.logger)
	if err != nil {
		a.logger.Fatal("error init db", zap.Error(err))
	}
	// инициализация мигратора
	migrator := migrate.NewMigrator(dbx, a.conf.DB, tableScanner)
	err = migrator.Migrate()
	if err != nil {
		a.logger.Fatal("migrator err", zap.Error(err))
	}

	// инициализация хранилищ
	newStorages := storages.NewStorages(sqlAdapter)
	a.Storages = newStorages

	var services *modules.Services
	// инициализация клиента для взаимодействия с сервисом пользователей
	if a.conf.RPCServer.Type == "JsonRPC" {
		client, err := jsonrpc.Dial("tcp", fmt.Sprintf("%s:%s", a.conf.UserRPC.Host, a.conf.UserRPC.Port))
		if err != nil {
			a.logger.Fatal("error init rpc client", zap.Error(err))
		}
		a.logger.Info("rpc client connected")

		services = modules.NewServices(newStorages, components, client)
	} else if a.conf.RPCServer.Type == "GRPC" {
		conn, err := grpc.Dial(fmt.Sprintf(
			"%s:%s", a.conf.UserRPC.Host, a.conf.UserRPC.Port),
			grpc.WithTransportCredentials(insecure.NewCredentials()))
		if err != nil {
			a.logger.Fatal("error init grpc client", zap.Error(err))
		}
		a.logger.Info("grpc client connected")

		client := upb.NewUsererClient(conn)

		services = modules.NewServices(newStorages, components, client)
	}

	// инициализация сервисов
	//services := modules.NewServices(rpcClient, newStorages, components)
	a.Servises = services

	// инициализация сервиса Auth в json RPC
	if a.conf.RPCServer.Type == "JsonRPC" {
		authRPC := auth.NewAuthServiceJSONRPC(services.Auth)
		jsonRPCServer := rpc.NewServer()
		err = jsonRPCServer.Register(authRPC)
		if err != nil {
			a.logger.Fatal("error init auth json RPC", zap.Error(err))
		}
		// инициализация сервера json RPC
		a.jsonRPC = server.NewJSONRPC(a.conf.RPCServer, jsonRPCServer, a.logger)
	} else if a.conf.RPCServer.Type == "GRPC" {
		gRPCServer := grpc.NewServer()

		authGRPC := auth.NewAuthServiceGRPC(services.Auth)
		apb.RegisterAutherServer(gRPCServer, authGRPC)

		a.jsonRPC = server.NewGRPC(a.conf.RPCServer, gRPCServer, a.logger)
	}

	controllers := modules.NewControllers(services, components)
	// инициализация роутера
	var r *chi.Mux
	r = router.NewRouter(controllers, components)
	// конфигурация сервера
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%s", a.conf.Server.Port),
		Handler: r,
	}
	// инициализация сервера
	a.srv = server.NewHttpServer(a.conf.Server, srv, a.logger)
	// возвращаем приложение
	return a
}
